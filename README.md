# SIGO Gateway

Projeto gateway para as aplicacoes do sistema SIGO.

## Configuracao

No arquivo de configuracao deve constar o mapeamento do contexto que
esta chegando para o contexto que sera redirecionado.

exemplo do arquivo de mapeamento/roteamento:

```
{
  "sigo-auth": {
    "host-redirect": "http://[HOST_REDIRECT]",
    "app-context": "/sigo-auth"
  },
  "login": {
    "host-redirect": "http://[HOST_REDIRECT]",
    "app-context": "/sigo-auth/login"
  },
  "sigo-auth-validate": {
    "host-redirect": "http://[HOST_REDIRECT]",
    "app-context": "/sigo-auth/token/valid"
  },
  "sigo-consult_asses": {
    "host-redirect": "http://[HOST_REDIRECT]",
    "app-context": "/sigo-consult_asses"
  }
}
```

## Redirecionamento

### Validacao

Com excecao do contexto de login, todas as outras requisicoes passam por validacao. Por conta disso, 
somente requisicoes com as devidas credenciais serao repassadas ao destino final.

### Solicitacao de login

Com o exemplo da acao de login, ao chegar o parametro "login" apos ao contexto, sera realizado um lookup no mapa de 
redirecionamento por login e em seguida sera realizada uma chamada com o devido redirecionamento.

Exemplo de chamada ao gateway.

**Request**
* GET /sigo-gateway/login HTTP/1.1
* Host: localhost:5001
* Header:
    * x-sigo-user: [USUARIO]
    * x-sigo-key: [KEY]

**Response**
* Status Code: HTTP/1.1 200 OK
* Body:

```
{
    "result": "[TOKEN]",
    "user": {
        "id": [USUARIO_ID],
        "name": "[USUARIO_NOME]",
        "user": "[USUARIO]",
        "active": [true/false]
    }
}
```

Exemplo de saida do gateway.

**Request**

* GET [app-context] HTTP/1.1
* Host: [host-redirect]
* Headers:
    * x-sigo-user: [USER]
    * x-sigo-key: [KEY]

**Response**

* Status Code: 200 - OK
* Body:

```
{
    "result": "[TOKEN]",
    "user": {
        "id": [USUARIO_ID],
        "name": "[NOME]",
        "user": "[NOME_USUARIO]",
        "active": [true/false]
    }
}
```

### Buscar Empresa

Com o exemplo da acao de buscar uma empresa por ID, ao chegar o parametro "sigo-consult_asses" apos ao contexto 
(sub-context), sera realizado um lookup no mapa de redirecionamento por "sigo-consult_asses" e em seguida sera 
realizada uma chamada com o devido redirecionamento.

Exemplo de chamada ao gateway.

**Request**
* GET /sigo-gateway/[sub-context: sigo-consult_asses]/empresa/?nome=Empresa HTTP/1.1
* Host: localhost:5001
* Header:
    * x-sigo-user: [USUARIO]
    * x-sigo-key: [KEY]

**Response**
* Status Code: HTTP/1.1 200 OK
* Body:

```
[ 
  { 
    "id":[EMPRESA_ID], 
    "cnpj":"[EMPRESA_CNPJ]", 
    "nome":"[EMPRESA_NOME]", 
    "telefone":"[EMPRESA_TELEFONE]" 
  } 
]

```

Exemplo de saida do gateway.

**Request**

* GET [app-context]/empresa/?nome=empresa HTTP/1.1
* Host: [host-redirect]
* Headers:
    * x-sigo-user: [USER]
    * x-sigo-key: [KEY]

**Response**

* Status Code: 200 - OK
* Body:

```
[ 
  { 
    "id":[EMPRESA_ID], 
    "cnpj":"[EMPRESA_CNPJ]", 
    "nome":"[EMPRESA_NOME]", 
    "telefone":"[EMPRESA_TELEFONE]" 
  } 
]
```
