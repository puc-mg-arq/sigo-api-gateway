package main

import (
	"bitbucket.org/puc-mg-arq/sigo-api-gateway/environment"
	"bitbucket.org/puc-mg-arq/sigo-api-gateway/handler"
	"bitbucket.org/puc-mg-arq/sigo-api-gateway/middleware"
	"bitbucket.org/puc-mg-arq/sigo-api-gateway/service"
	"bitbucket.org/puc-mg-arq/sigo-api-gateway/util"
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi"
	"github.com/kelseyhightower/envconfig"
	"log"
	"net/http"
	"os"
	"time"
)

var logging util.Logging

func init() {

	logger := log.New(os.Stdout, "", log.Flags())
	logging = util.Logging{
		Logger: logger,
	}

	err := envconfig.Process("setting", &environment.Setting)
	if err != nil {
		logging.LoggerMessage("fail to load environment config", "error", err.Error())
		panic(err.Error())
	}

	logging.LoggerMessage("settings loaded", "json_config", string(environment.Setting.GetJson()))

}

func main() {

	httpClientService := GetNewClient()
	httpClientValidator := GetNewClient()

	serverService := service.NewServerService(&httpClientService, logging)

	serverPass := &handler.ServerPass{
		ServerService: &serverService,
		Logging:       logging,
	}

	validate := &middleware.Validate{
		ServerService: &serverService,
		Logging:       logging,
		Client:        &httpClientValidator,
	}

	loggingMiddleware := &middleware.Logging{
		Logging: logging,
	}

	router := chi.NewRouter()
	//context := "sigo-gateway"
	context := environment.Setting.Server.Context

	router.With(loggingMiddleware.Request).HandleFunc(fmt.Sprintf("/%s/{context:login}", context), serverPass.Server)

	router.With(loggingMiddleware.Request, validate.ValidateCredentials).Route(fmt.Sprintf("/%s", context), func(r chi.Router) {

		r.HandleFunc("/{context}/{\\/}*", serverPass.Server)

		r.HandleFunc("/health", func(writer http.ResponseWriter, request *http.Request) {
			marshal, _ := json.Marshal(map[string]string{"status": "okay"})
			writer.Header().Add("Content-Type", "application/json")
			writer.WriteHeader(http.StatusOK)
			writer.Write(marshal)
		})
	})

	serverHttp := &http.Server{
		Addr:           environment.Setting.Server.Port,
		Handler:        router,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	logging.LoggerMessage("Server started", "port", serverHttp.Addr, "context", context)
	if err := serverHttp.ListenAndServe(); err != nil {
		logging.LoggerMessage("Listen and Serve", "err", err.Error())
		panic(err.Error())
	}

}

func GetNewClient() http.Client {

	newHttpClient := util.NewHttpClient(environment.Setting.Connection.RequestTimeout,
		environment.Setting.Connection.MaxIdleConnections, environment.Setting.Connection.IdleConnectionTimeout)

	return newHttpClient
}
