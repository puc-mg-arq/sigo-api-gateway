package handler

import (
	"bitbucket.org/puc-mg-arq/sigo-api-gateway/service"
	"bitbucket.org/puc-mg-arq/sigo-api-gateway/util"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"
)

type ServerPass struct {
	ServerService *service.Server
	Logging       util.Logging
}

func (h *ServerPass) Server(w http.ResponseWriter, r *http.Request) {

	logger := h.Logging.LoggerMessageDefault("request_host", r.Host, "request_path", r.URL.Path,
		"request_rawquery", r.URL.RawQuery, "user", r.Header.Get("x-sigo-user"), "request_method", r.Method,
		"request_protocol", r.Proto, "token", r.Header.Get("x-sigo-token"))

	startElapseTime := time.Now()
	response, err := h.ServerService.Call(r)
	if err != nil {
		logger.LoggerMessage("error ao executar server", "error", err.Error())
		marshal, _ := json.Marshal(map[string]string{"error": err.Error()})
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(marshal)
		return
	}

	defer response.Body.Close()

	util.CopyHeader(w.Header(), response.Header)
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}

	logger.LoggerMessage("server response", "status_code", response.StatusCode, "body", string(body),
		"elapse_time", time.Now().Sub(startElapseTime).Seconds())

	w.WriteHeader(response.StatusCode)
	w.Write(body)
}
