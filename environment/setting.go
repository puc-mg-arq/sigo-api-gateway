package environment

import (
	"encoding/json"
	"time"
)

type setting struct {
	Server struct {
		Context string `envconfig:"SERVER_CONTEXT" default:"sigo-gateway"`
		Port    string `envconfig:"PORT" default:":5001" required:"true" ignored:"false"`
	}
	Connection struct {
		MaxIdleConnections    int           `envconfig:"CONNECTION_MAX_IDLE_CONNECTIONS" default:"100"`
		IdleConnectionTimeout time.Duration `envconfig:"CONNECTION_MAX_IDLE_CONNECTIONS" default:"3s"`
		RequestTimeout        time.Duration `envconfig:"CONNECTION_REQUEST_TIMEOUT" default:"3s"`
	}
}

var Setting setting

func (set *setting) GetJson() []byte {

	data, err := json.Marshal(set)
	if err != nil {
		data, _ = json.Marshal(setting{})
	}

	return data
}
