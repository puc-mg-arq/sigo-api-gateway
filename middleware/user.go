package middleware

import (
	"bitbucket.org/puc-mg-arq/sigo-api-gateway/service"
	"bitbucket.org/puc-mg-arq/sigo-api-gateway/util"
	"fmt"
	"io/ioutil"
	"net/http"
)

type Validate struct {
	ServerService *service.Server
	Logging       util.Logging
	Client        *http.Client
}

// https://github.com/go-chi/chi
func (v *Validate) ValidateCredentials(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		url := getURLToValidateCredentials(v.ServerService.Config)

		logger := v.Logging.LoggerMessageDefault("request_host", r.Host, "request_path", r.URL.Path,
			"request_rawquery", r.URL.RawQuery, "user", r.Header.Get("x-sigo-user"), "request_method", r.Method,
			"request_protocol", r.Proto, "url_request", url)

		request, err := http.NewRequest(http.MethodGet, url, nil)
		util.CopyHeader(request.Header, r.Header)
		if err != nil {
			logger.LoggerMessage("validate credentials", "error", err.Error())
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		response, err := v.Client.Do(request)
		if err != nil {
			logger.LoggerMessage("validate credentials", "error", err.Error())
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		defer response.Body.Close()
		if response.StatusCode != 200 {

			util.CopyHeader(w.Header(), response.Header)
			body, err := ioutil.ReadAll(response.Body)
			if err != nil {
				logger.LoggerMessage("validate credentials - reading body", "status_code", response.StatusCode,
					"error", err.Error())
				http.Error(w, err.Error(), response.StatusCode)
				return
			}

			logger.LoggerMessage("invalid credentials", "status_code", response.StatusCode,
				"body_message", string(body))
			http.Error(w, string(body), response.StatusCode)
			return
		}

		logger.LoggerMessage("success to validate credentials", "status_code", response.StatusCode)
		next.ServeHTTP(w, r)
	})
}

func getURLToValidateCredentials(config map[string]map[string]string) string {

	hostRedirect := config["sigo-auth-validate"]["host-redirect"]
	appContext := config["sigo-auth-validate"]["app-context"]

	url := fmt.Sprintf("%s%s", hostRedirect, appContext)

	return url
}
