package middleware

import (
	"bitbucket.org/puc-mg-arq/sigo-api-gateway/util"
	"net/http"
)

type Logging struct {
	Logging util.Logging
}

// https://github.com/go-chi/chi
func (l *Logging) Request(next http.Handler) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		l.Logging.LoggerMessage("log request", "request_host", r.Host, "request_path", r.URL.Path,
			"request_rawquery", r.URL.RawQuery, "user", r.Header.Get("x-sigo-user"), "request_method", r.Method,
			"request_protocol", r.Proto)

		next.ServeHTTP(w, r)
	})
}
