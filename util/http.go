package util

import (
	"net/http"
	"time"
)

func NewHttpClient(timeout time.Duration, maxIdleConns int, idleConnTimeout time.Duration) http.Client {
	return http.Client{
		Timeout: timeout,
		Transport: &http.Transport{
			MaxIdleConns:       maxIdleConns,
			IdleConnTimeout:    idleConnTimeout,
			DisableCompression: true,
		},
	}
}

func CopyHeader (to http.Header, from http.Header) {
	for key, value := range from {
		for _, v := range value {
			to.Set(key, v)
		}
	}
}