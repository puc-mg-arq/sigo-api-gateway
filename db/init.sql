create schema sigo_auth

-- auto-generated definition
    create table token
    (
        id serial       not null
            constraint sigo_token_pkey
                primary key,
        token        varchar(255) not null,
        user_id      varchar(255) not null,
        date_created timestamp default timezone('BRT'::text, now())
    )

--alter table token
--    owner to sigo

create
unique index sigo_token_id_uindex
    on token (id)

create table user_auth
(
    id           serial       not null
        constraint sigo_user_pkey
            primary key,
    name         varchar(255) not null,
    username     varchar(255) not null,
    key          varchar(255) not null,
    date_created timestamp default timezone('BRT'::text, now()),
    active       bool default false
)

create
unique index sigo_user_auth_id_uindex
    on user_auth (id);

INSERT INTO sigo_auth.user_auth (name, username, key, active) VALUES ('Mairon Costa', 'mairon', '12345', '1');