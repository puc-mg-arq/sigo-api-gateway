module bitbucket.org/puc-mg-arq/sigo-api-gateway

go 1.15

require (
	github.com/go-chi/chi v1.5.1
	github.com/kelseyhightower/envconfig v1.4.0
)
