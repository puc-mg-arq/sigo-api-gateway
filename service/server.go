package service

import (
	"bitbucket.org/puc-mg-arq/sigo-api-gateway/util"
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi"
	"net/http"
	"strings"
	"time"
)

const (
	FileName = "router_exemple.json"
)

type Server struct {
	Config  map[string]map[string]string
	Client  *http.Client
	Logging util.Logging
}

func NewServerService(clientService *http.Client, logging util.Logging) Server {
	s := Server{
		Config:  loadConfig(),
		Client:  clientService,
		Logging: logging,
	}

	return s
}

func loadConfig() map[string]map[string]string {

	var config map[string]map[string]string
	mockPath := util.GetMockPath()
	jsonFile := util.GetFile(mockPath, FileName)
	err := json.Unmarshal(jsonFile, &config)

	if err != nil {
		panic(err)
	}

	return config
}

func (service *Server) Call(r *http.Request) (*http.Response, error) {

	realPathToForward := service.getRealPathToForward(r)

	logger := service.Logging.LoggerMessageDefault("request_host", r.Host, "request_path", r.URL.Path,
		"request_rawquery", r.URL.RawQuery, "user", r.Header.Get("x-sigo-user"), "request_method", r.Method,
		"request_protocol", r.Proto, "token", r.Header.Get("x-sigo-token"), "real_path_to_forward", realPathToForward)

	startElapseTime := time.Now()

	request, err := http.NewRequest(r.Method, realPathToForward, r.Body)
	util.CopyHeader(request.Header, r.Header)
	if err != nil {
		return nil, err
	}

	response, err := service.Client.Do(request)
	if err != nil {
		logger.LoggerMessage("erro ao executar chamada no server", "erro", err.Error(), "elapse_time", time.Now().Sub(startElapseTime).Seconds())
		return nil, err
	}

	logger.LoggerMessage("success to execute server call", "elapse_time", time.Now().Sub(startElapseTime).Seconds())

	return response, nil
}

func (service *Server) getRealPathToForward(r *http.Request) string {

	context := chi.URLParam(r, "context")
	hostRedirect := service.Config[context]["host-redirect"]
	appContext := service.Config[context]["app-context"]

	splitPath := strings.Split(r.URL.Path, "/")
	i := splitPath[3:]
	path := fmt.Sprintf("%s%s", hostRedirect, appContext)

	if url := strings.Join(i, "/"); len(url) > 1 {
		path = fmt.Sprintf("%s/%s", path, url)
	}

	if len(r.URL.RawQuery) > 0 {
		path += fmt.Sprintf("?%s", r.URL.RawQuery)
	}

	return path
}
